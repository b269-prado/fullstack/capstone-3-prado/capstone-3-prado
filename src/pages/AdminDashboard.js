import {useState, useEffect, useContext} from 'react';

import AllProducts from '../components/AllProducts';

import UserContext from '../UserContext';

import {Link} from 'react-router-dom';
import {Button, Table} from 'react-bootstrap';

export default function AdminDashboard() {

	const [allProducts, setAllProducts] = useState([]);	

	useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllProducts(data)
		})

	}, [])

	return (
		<>
		<Button className="btn-primary" as={Link} to='/products/create'>Create New Product</Button>
		{/*<Button className="btn-success" as={Link} to='/orders/all'>Show User Orders</Button>*/}
		<Table striped bordered hover>
	        <thead>
	          <tr>
	            <th>Name</th>
	            <th>Description</th>
	            <th>Price</th>
	            <th>Availability</th>
	            <th>Actions</th>
	          </tr>
	        </thead>
	        {allProducts.map(product => <AllProducts key={product._id} product={product} />)}
	    </Table>
		</>
	)
}

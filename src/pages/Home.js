import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Sweet Trolley",
		content: "Your one-stop shop for magic sweets.",
		destination: "/products",
		label: "View Products!"
	}


	return (
		<>
		<Banner data={data} />
    	{/*<Highlights />*/}
    	
		</>
	)
}
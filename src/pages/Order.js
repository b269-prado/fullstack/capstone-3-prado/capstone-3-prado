import { useState, useEffect } from 'react';

import OrderCard from '../components/OrderCard';

export default function Order() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/retrive`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setOrders(data);
      });
  }, []);

  return (
    <>
      {orders.map(order => (
        <OrderCard key={order._id} order={order} />
      ))}
    </>
  );
}

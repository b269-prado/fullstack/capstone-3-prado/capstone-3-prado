import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import {Button, Table} from 'react-bootstrap';

import CartItem from '../components/CartItem';

export default function Cart() { 

    const navigate = useNavigate();
    

    const [carts, setCarts] = useState({ products: [] });
    const [order, setOrder] = useState([]);


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/carts/retrieve`, {
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setCarts(data);
        })
    }, [])

    const checkOut = () => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }            
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setOrder();

            if (data === false) {

                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
                
            } else {
                Swal.fire({
                    title: "Product/s successfully ordered.",
                    icon: "success",
                    text: "Go back to Products page."
                })

                navigate("/products");                
            }
        })
    };

    return(
        <>
            <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Name</th>                    
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                {carts.products && carts.products.map(cart => <CartItem key={cart._id} cart={cart} />)}
            </Table>
            <Button className="btn-primary" onClick={() => checkOut()} >Checkout</Button>
        </>
    )
};
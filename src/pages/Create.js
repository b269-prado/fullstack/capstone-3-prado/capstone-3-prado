import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

export default function Create() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	
	const [name, setName] = useState("");	
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState(false);

    useEffect(() => {
	    if(name !== "" && price !== ""  && description !== "") {
	        setIsActive(true)
	    } else {
	        setIsActive(false)
	    }
	}, [name, price, description]);

	function addProduct(e) {
		e.preventDefault();

		console.log(user);
		if(user.isAdmin) {
			fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if (data === true) {

					// Clear input fields
					setName("");
					setDescription("");	
					setPrice("");									

					Swal.fire({
						title: "Product successfully created.",
						icon: "success",
						text: "Product has been added to catalog."
					})

					navigate("/dashboard");

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}
			})
		}
	};

	return (
		<Form onSubmit={(e) => addProduct(e)} >

		  <Form.Group className="mb-3" controlId="name">
		    <Form.Label>Product Name</Form.Label>
		    <Form.Control 
		        type="text"
		        value={name}
		        onChange={(e) => {setName(e.target.value)}}
		        placeholder="Enter product name" 
		        required
		        />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="description">
		    <Form.Label>Product Description</Form.Label>
		    <Form.Control 
		        type="text"
		        value={description}
		        onChange={(e) => {setDescription(e.target.value)}}
		        placeholder="Enter product description" />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="price">
		    <Form.Label>Product Price</Form.Label>
		    <Form.Control 
		        type="text"
		        value={price}
		        onChange={(e) => {setPrice(e.target.value)}}
		        placeholder="Enter product price" />		    
		  </Form.Group>
		  
		  { isActive ?
	            <Button variant="primary" type="submit" id="submitBtn">
	             Create
	            </Button>
	            :
	            <Button variant="primary" type="submit" id="submitBtn" disabled>
	              Create
	            </Button>
		  }		 
		</Form> 
	)

}
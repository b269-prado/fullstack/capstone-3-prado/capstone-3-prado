import {useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function MainNavbar() {
  
  const {user} = useContext(UserContext);

  return (

    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">Sweet Trolley</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto"> 
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to='/products'>Products</Nav.Link>
            { (user.id !== null) ? 
            <>
            <Nav.Link as={Link} to='/carts/retrieve'>Cart</Nav.Link>
            {/*<Nav.Link as={Link} to='/orders/retrieve'>Order</Nav.Link>*/}
            <Nav.Link as={Link} to='/logout'>Logout</Nav.Link>            
            </>
            :
            <>          
            <Nav.Link as={Link} to='/login'>Login</Nav.Link>    
            <Nav.Link as={Link} to='/register'>Register</Nav.Link>
            {/*<Nav.Link as={Link} to='/cart'>Cart</Nav.Link>*/}
            </>            
            }            
          </Nav>
        </Navbar.Collapse>
    </Navbar>

  );
}
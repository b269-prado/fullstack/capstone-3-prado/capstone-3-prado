import {Link, NavLink} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AdminNavbar() {
  


  return (

    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">Sweet Trolley</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto"> 
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>            
            <Nav.Link as={Link} to='/dashboard'>Admin Dashboard</Nav.Link>            
            <Nav.Link as={Link} to='/logout'>Logout</Nav.Link>            
          </Nav>
        </Navbar.Collapse>
    </Navbar>

  );
}
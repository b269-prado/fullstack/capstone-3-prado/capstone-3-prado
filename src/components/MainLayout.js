import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from 'react-bootstrap';

import MainNavbar from './MainNavbar';
import ProductView from './ProductView';

import Home from '../pages/Home';
import Products from '../pages/Products';
import Register from '../pages/Register';
import Login from '../pages/Login';
import Logout from '../pages/Logout';
import Cart from '../pages/Cart';
import Order from '../pages/Order';
import Error from '../pages/Error';

export default function MainLayout() {



	return (

		<Router>
		  < MainNavbar/>
		  <Container>
		  <Routes>
		    < Route path="/" element={<Home/>}/>
		    < Route path="/products" element={<Products/>}/>
		    < Route path="/products/:productId" element={<ProductView/>}/>		              
		    < Route path="/register" element={<Register/>}/>
		    < Route path="/login" element={<Login />}/>
		    < Route path="/logout" element={<Logout/>}/>
		    < Route path="/orders/retrieve" element={<Order/>}/>
		    < Route path="/carts/retrieve" element={<Cart/>}/>
		    < Route path="/*" element={<Error />} />
		  </Routes>
		  </Container>
		</Router>  

	)
}


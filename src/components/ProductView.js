import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const {user} = useContext(UserContext);
	const [quantity, setQuantity] = useState(0);

	const navigate = useNavigate();	
	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

const addToCart = () => {
	fetch(`${process.env.REACT_APP_API_URL}/carts/add`, {
		method: "POST",
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			productId: productId,
			quantity: quantity
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(data === false) {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})

		} else {

			Swal.fire({
				title: "Successfully added to cart.",
				icon: "success",
				text: "Product has been added to cart."
			})

			navigate("/products")
			
		}

	})

	setQuantity("");
};

function incrementQuantity() {  
  setQuantity(quantity + 1);
}
function decrementQuantity() {  
  setQuantity(quantity - 1);
}

useEffect(() => {
	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
}, [productId, quantity])

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Quantity:</Card.Subtitle>
					        <Card.Text>
					        <span className="App">
					          <button onClick={decrementQuantity}>-</button>
					          <span>  {quantity}  </span>
					          <button onClick={incrementQuantity}>+</button>
					        </span>
					        </Card.Text>

					        
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" onClick={() => addToCart(productId, quantity)} >Add to Cart</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Add to Cart</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
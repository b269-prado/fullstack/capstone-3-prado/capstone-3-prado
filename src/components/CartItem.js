import {ProductProvider} from '../ProductContext';

import {useState} from 'react';

export default function CartItem({cart}) {

    const [ products, setProducts ] = useState([]); 
   
    const { name, price, quantity, subtotal  } = cart;

    return (
        <ProductProvider value={{ products, setProducts }}>
            <tbody>
                <tr>
                  <td>{name}</td>                  
                  <td>{price}</td>
                  <td>{quantity}</td>
                  <td>{subtotal}</td>
                </tr>            
            </tbody>
        </ProductProvider>
    )
}
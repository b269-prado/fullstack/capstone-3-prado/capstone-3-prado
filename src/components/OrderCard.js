import { useState } from 'react';
import { Table } from 'react-bootstrap';
import { Row, Col, Card } from 'react-bootstrap';

import { ProductProvider } from '../ProductContext';

export default function OrderCard({ order }) {
  const { _id, products, amount, purchasedOn } = order;
  const [orders, setOrders] = useState([]);

  return (
    <ProductProvider value={{ orders, setOrders }}>
      <Row className="mt-3 mb-3">
        <Col sm={12}>
          <Card className="cardHighlight p-0">
            <Card.Body>
              <Card.Title>
                <h4>Order ID: {_id}</h4>
              </Card.Title>
              <Card.Text>Order Date: {purchasedOn}</Card.Text>
              <Card.Subtitle>Order Total Amount: {amount}</Card.Subtitle>
              <Table striped>
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  {products.map(product => (
                    <tr key={product.productId}>
                      <td>{product.name}</td>
                      <td>{product.price}</td>
                      <td>{product.quantity}</td>
                      <td>{product.subtotal}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </ProductProvider>
  );
}

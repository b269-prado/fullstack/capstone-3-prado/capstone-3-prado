import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from 'react-bootstrap';

import AdminNavbar from './AdminNavbar';
import AdminDashboard from '../pages/AdminDashboard';
import Home from '../pages/Home';
import Create from '../pages/Create';
import UpdateProduct from './UpdateProduct';
import Login from '../pages/Login';
import Logout from '../pages/Logout';
import Error from '../pages/Error';

export default function AdminLayout() {

	return (

		<Router>
		  < AdminNavbar/>
		  <Container>
		  <Routes>
		    < Route path="/" element={<Home/>}/>		    
		    < Route path="/dashboard" element={<AdminDashboard/>}/>		    
		    < Route path="/products/create" element={<Create/>}/>
		    < Route path="/products/update/:productId" element={<UpdateProduct/>}/>
		    < Route path="/login" element={<Login />}/>
		    < Route path="/logout" element={<Logout/>}/>
		    < Route path="/*" element={<Error />} />
		  </Routes>
		  </Container>
		</Router>  

	)
}



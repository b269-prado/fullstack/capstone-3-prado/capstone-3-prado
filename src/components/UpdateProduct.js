import { useState, useEffect, useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";

import UserContext from '../UserContext';

import { Button, Form } from "react-bootstrap";

import Swal from 'sweetalert2';

export default function UpdateProduct() {
	const navigate = useNavigate();

	const {user} = useContext(UserContext);
	
	const {productId} = useParams();    

	const [name, setName] = useState("");	
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);		

    useEffect(() => {
    if(name !== "" && description !== "" && price !== "") {
        setIsActive(true)
    } else {
        setIsActive(false)
    }    
}, [name, description, price])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(product => {
                setName(product.name);
                setDescription(product.description);
                setPrice(product.price);
            })
            .catch(error => {
                console.log(error);
            });
    }, [productId]);

    function updateProduct (e) {
        e.preventDefault();   	

    	console.log(user);
    	if(user.isAdmin) {
    		fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
    			method: "PUT",
    			headers: {
    				'Content-Type': 'application/json',
    				Authorization: `Bearer ${localStorage.getItem('token')}`
    			},
    			body: JSON.stringify({
    				name: name,
    				description: description,
    				price: price
    			})
    		})
    		.then(res => res.json())
    		.then(data => {
    			console.log(data)

    			if (data === true) {

    				// Clear input fields
    				setName("");
    				setDescription("");	
    				setPrice("");

    				Swal.fire({
    					title: "Product successfully updated.",
    					icon: "success",
    					text: "Product entry has been updated in catalog."
    				})

    				navigate("/dashboard");

    			} else {    				

    				Swal.fire({
    					title: "Something went wrong",
    					icon: "error",
    					text: "Please try again."
    				})
    			}
    		})
    	}
    };

    return (
    	<>
        <Form onSubmit={(e) => updateProduct(e)} >

          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Product Name</Form.Label>
            <Form.Control 
                type="text"
                value={name}
                onChange={(e) => {setName(e.target.value)}}
                placeholder="Enter product name" 
                required
                />
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Product Description</Form.Label>
            <Form.Control 
                type="text"
                value={description}
                onChange={(e) => {setDescription(e.target.value)}}
                placeholder="Enter product description" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="price">
            <Form.Label>Product Price</Form.Label>
            <Form.Control 
                type="text"
                value={price}
                onChange={(e) => {setPrice(e.target.value)}}
                placeholder="Enter product price" />            
          </Form.Group>
          
          { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                 Update
                </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                  Update
                </Button>
          }      
        </Form>
    	</>
    )
};
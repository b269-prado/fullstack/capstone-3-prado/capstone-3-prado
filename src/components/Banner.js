import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from "react";

import UserContext from '../UserContext';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

    const {user} = useContext(UserContext);

return (
    <Row>
        <Col className="p-5">
            <h1>{title}</h1>
            <p>{content}</p>
            { user.isAdmin ? 
            <Button variant="primary" as={Link} to={`/dashboard`} >Go to Admin Dashboard</Button>
            :         
            <Button variant="primary" as={Link} to={destination} >{label}</Button>
            }
        </Col>
    </Row>
    )
}
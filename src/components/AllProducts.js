import {ProductProvider} from '../ProductContext';
import {useState} from 'react';
import {Link, useParams} from 'react-router-dom';
import {Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AllProducts({product}) {

    const [ products, setProducts ] = useState([]);         
  
    const { name, description, price, _id, isActive } = product;

    const archiveProduct = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }            
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.isActive !== undefined) {
                setProducts(prevProducts => prevProducts.map(product => {
                    if(product._id === data._id) {
                        return {
                            ...product,
                            isActive: data.isActive
                        };
                    }
                    return product;
                }));
                
            } else {
                Swal.fire({
                    title: "Availability status has been changed.",
                    icon: "success",
                    text: "Product catalog updated."
                }) 
            }           
        })
        .catch(error => {
            console.log(error);
            Swal.fire({
                title: "Error",
                icon: "error",
                text: "Please try again."
            }) 
        });
    };

return (

    <ProductProvider value={{ products, setProducts }}>
        <tbody>
            <tr>
              <td>{name}</td>
              <td>{description}</td>
              <td>{price}</td>
              <td>{isActive ? "Available" : "Unavailable"}</td>
              <td>
              <div><Button className="btn-primary" as={Link} to={`/products/update/${_id}`}>Update</Button></div>
              <div><Button className="btn-danger" onClick={() => archiveProduct(`${_id}`)}>
              {isActive ? "Deactivate" : "Reactivate"}
              </Button></div>          
              </td>
            </tr>            
        </tbody>
    </ProductProvider>
    )
};
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext';
import MainLayout from "./components/MainLayout";
import AdminLayout from "./components/AdminLayout";

import './App.css'; 

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
          
          if(typeof data._id !== "undefined") {

              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          }          
          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }

      })
  }, []);
  return (    
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        { user.isAdmin ? <AdminLayout/> : <MainLayout/> }    
      </UserProvider>
    </>
  );
}

export default App;


